import styled from "styled-components"
import { FlexDiv } from "../../styles/common"

export const AppWrapper = styled(FlexDiv)`
  width: 100%;
  height: 100%;
  flex-direction: column;
  background-color: ${({theme}) => theme.bgColor};
  color: ${({theme}) => theme.fontColor};
`;