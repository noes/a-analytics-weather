import styled from "styled-components"

export const FlexDiv = styled.div`
  display: flex;
  align-items:  ${({align}) => align || "center"};
  justify-content: ${({jc}) => jc || "center"};
  flex-direction: ${({direction}) => direction || "column"};
`;
